---
layout: post
title:  "Jotun - Art style vs. Graphics"
date:   2018-02-08 21:47:31 +0000
categories: games
author: msandreereid
---
Video game graphics have been steadily increasing in raw graphical power for decades. Notable companies pushing these graphical enhancements include DICE and Activision, with their franchises Battlefield and Call of Duty respectively.

![Battlefield 1 graphics]({{"/assets/images/artvsgraphics/bf1graphics.jpg" | absolute_url }})
#### Battlefield 1

Even though these games are undeniably beautiful I think they don't even come close to being as breathtaking as a game like [Jotun](http://jotungame.com/). This top-down boss-battler/exploration game, while not having the greatest gameplay, does have an absolutely incredible hand-drawn art style, like the one seen in [Cuphead](https://www.cuphead.org/).
This art combined with the excellent camera movements at scenic spots creates moments that are only surpassed in beauty in video games by scenes from a similar game, [Hyper Light Drifter](http://www.heart-machine.com/), and a fully cinematic experience, [Firewatch](http://www.firewatchgame.com/).

![Jotun cinematics]({{ "/assets/images/artvsgraphics/jotuncinematics.jpg" | absolute_url }})
#### Jotun Boss Battle

It was scenes such as these that kept me coming back for more even when the gameplay was tedious or infuriatingly hard. Because of this, and the sometimes excellent gameplay and epic boss battles, I would recommend this game to anyone with even a shred of patience.
