---
layout: post
title: "Recent pixel art and low poly graphics in games - authenticity vs. beauty"
date: 2018-02-05 19:55:00
categories: games
author: JoeJ1
---
In recent years pixel art and low poly graphics in video games have become increasingly popular, both because of their (relatively) low difficulty and cost to produce for indies, and their appealing retro feel. The pixel art of the majority of today's games does not look as though it could have been from a real game from the eighties. This is primarily because of greater colour palette possibilities (with artists no longer being limited to just 256), and improved techniques and tools. Does this mean low poly and pixel art games have lost their authenticity or are we merely improving over the years?

Part of the reason this is a difficult question to answer is that the primary appeal of low poly and pixel art graphics is their nostalgic appeal. Naturally anything that brings the viewer away from this feeling will detract from the art's value. A reason why this is often not noticed by players is because of their false memories of the true quality of these older games. Very often they forget how poor, in comparison to today's graphics, these games' visuals truly were. This is probably why inaccurate pixel art games are often not noticed as such.

The improvement and refinement of these forms of art is not at all a problem and, as the medium of video games emerges as a whole, all of its aspects, even those with a nostalgic appeal, should improve. Many of the more recent pixel art and low poly games seem to have almost completely lost their connection to the older games, which has benefited them. While constraints often lead to more creativity, occasionally the shackles are too tight, and over the years originality is lost. One example of beautiful pixel art in games is the brilliant [Owlboy](http://www.owlboygame.com/). While this game does still appeal to the nostalgia in its players, its art style has evolved far beyond its predecessors.

In conclusion, the improvements and changes in pixel art and low poly art are just that: improvements. The advancement of anything in technology is rarely a mistake and these recent games' beautiful art is no exception. I look forward to seeing what amazing low poly and pixel art the future brings.
