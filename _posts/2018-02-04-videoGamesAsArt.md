---
layout: post
title: "Why video games are art - and why they aren't"
date: 2018-02-04 19:55:00
categories: games
author: JoeJ1
---
The question of whether or not video games are art has been a hotly debated issue for decades now, with valid and interesting arguments from both sides. This article is about the reasons *for* video games as an artform, reasons and examples of video games that aren't art, and why that's okay.

Every artform should bring something new, a new way of looking at and analysing the human experience. Painting, drawing, etc. are the artforms of sight; music is the artform of sound; film is the artform of sight and sound. Video games are the artform of sight, sound, systems, and interaction. In this aspect video games, I believe, are certainly an artform. It has more new, interesting aspects than most other artforms and the fact that it uses similar aspects as film does not demerit it's value as an artform. My reasoning for this is that film, too, borrows from other artforms and very few argue that film is not an artform.

This is not to say, however, that all games are art. One wouldn't argue that games like [pong](https://en.wikipedia.org/wiki/Pong) or [CS:GO](https://en.wikipedia.org/wiki/Counter-Strike:_Global_Offensive) are art. The fact that not *all* games are art does not detract from it's value as an artform, in the same way not every pot being a beautiful sculpture does not mean sculptures are not art. There are countless examples of artistic video games that use the artform's unique ability to put the player in the footsteps of another and feel the weight of action. A few of these being: [Proteus](https://en.wikipedia.org/wiki/Proteus_(video_game)), a beautiful exploration game with no goal, allowing the player to freely explore a procedurally-generated landscapes filled with amazing sounds and sights; [The Witness](https://en.wikipedia.org/wiki/The_Witness_(2016_video_game)), a 3D puzzle game made by Jonathan Blow, developer of Braid, which explores the interaction with line puzzles to it's fullest extent while being contained in a beautiful open world with an ambiguous story; [Papers, Please](https://en.wikipedia.org/wiki/Papers,_Please), a puzzle game that through only the interaction of checking documents at a border control station gives the player powerful and thought-provoking moral dilemas.

In conclusion, video games are almost certainly an artform, and one with great potential and opportunity. The fact that many games are not art does not detract from their value overall. Games are almost certainly the most recently created artform and acknowledging it as one helps people to better themselves and others.
