---
layout: page
title: About
permalink: /about/
---

# Welcome to the Cereal Bus tech blog.
A tech blog created by [@JoeJ1](https://gitlab.com/JoeJ1) and [@msandreereid](https://gitlab.com/msandreereid) that features games, Linux, and everything to do with tech!
